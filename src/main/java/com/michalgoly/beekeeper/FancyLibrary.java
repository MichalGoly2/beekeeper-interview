package com.michalgoly.beekeeper;

import java.util.ArrayList;
import java.util.Collection;

public class FancyLibrary {

    public static <T>Collection extract(Collection<T> collection, Rule<T> worker) {
        Collection<T> out = new ArrayList<>();
        for (T t : collection)
            if (worker.checkRule(t))
                out.add(t);
        return out;
    }

}
