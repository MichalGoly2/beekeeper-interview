package com.michalgoly.beekeeper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Interview {

    public static void main(String[] args) {
        FancyLibrary library = new FancyLibrary();
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(5);
        list.add(1);
        list.add(2);
        Collection<Integer> lis2t = library.extract(list, new Rule<Integer>() {
            @Override
            public boolean checkRule(Integer integer) {
                return integer % 2 != 0;
            }
        });
        for (Integer i : lis2t)
            System.out.print(i + " ");
    }

}
