package com.michalgoly.beekeeper;

public class EvenNumWorker implements Rule<Integer> {

    @Override
    public boolean checkRule(Integer integer) {
        return integer % 2 == 0;
    }
}
