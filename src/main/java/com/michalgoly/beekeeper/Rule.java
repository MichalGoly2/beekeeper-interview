package com.michalgoly.beekeeper;

import java.util.Collection;

public interface Rule<T> {

//    Collection<T> extract(Collection<T> collection);
    boolean checkRule(T t);
}
