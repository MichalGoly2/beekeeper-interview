package com.michalgoly.beekeeper;


import java.util.Collection;
import java.util.Iterator;

public class EvenNumberIterator {

    private Iterator<Integer> iterator = null;
    private Integer current = null;

    public EvenNumberIterator(Collection<Integer> collection) {
        this.iterator = collection.iterator();
        current = getNextEvenNumber();
    }

    public boolean hasNext() {
        return current != null;
    }

    public Integer next() throws Exception {
        if (hasNext()) {
            Integer out = current;
            current = getNextEvenNumber();
            return out;
        } else {
            throw new Exception("d");
        }
    }

    public Integer getNextEvenNumber() {
        while (this.iterator.hasNext()) {
            Integer temp = this.iterator.next();
            if (temp % 2 == 0) {
                current = temp;
                return current;
            }
        }
        return null;
    }



}
